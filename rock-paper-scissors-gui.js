let playerWins = 0;
let computerWins = 0;

function computerPlay() {
    let choices = ['Rock', 'Paper', 'Scissors'];
    return choices[Math.floor(Math.random() * choices.length)];
}

function playRound(playerSelection, computerSelection) {
    playerSelection = playerSelection.toLowerCase();
    computerSelection = computerSelection.toLowerCase();

    console.info("Player: " + playerSelection);
    console.info("Computer: " + computerSelection);


    if (playerSelection === 'rock' && computerSelection === 'scissors'){
        playerWins += 1;
        return "You win! Rock beats Scissors."
    } else if (playerSelection === 'paper' && computerSelection === 'rock') {
        playerWins += 1;
        return "You win! Paper beats Rock."
    } else if (playerSelection === 'scissors' && computerSelection === 'paper') {
        playerWins += 1;
        return "You win! Scissors beats Paper."
    } else if (playerSelection === computerSelection) {
        return "It's a draw!"
    } else {
        computerWins += 1;
        return `You lose! ${computerSelection} beats ${playerSelection}`
    }
}

function game() {
    let playerSelection;
    const playerOptions = document.querySelectorAll(".player-option");

    let computerSelection = computerPlay();

    let roundOutcome = document.querySelector('#round-outcome');
    let score = document.querySelector('#score');

    for (let i = 0; i < playerOptions.length; i++) {
        playerOptions[i].addEventListener('click', (e) => {
            console.log(`${i} was clicked.`);
            switch (i) {
                case 0:
                    playerSelection = 'rock';
                    computerSelection = computerPlay();
                    roundOutcome.textContent = `${playRound(playerSelection, computerSelection)}`;
                    score.textContent = `You: ${playerWins} | CPU: ${computerWins}`;
                    break;
                case 1:
                    playerSelection = 'paper';
                    computerSelection = computerPlay();
                    roundOutcome.textContent = `${playRound(playerSelection, computerSelection)}`;
                    score.textContent = `You: ${playerWins} | CPU: ${computerWins}`;
                    break;
                case 2:
                    playerSelection = 'scissors';
                    computerSelection = computerPlay();
                    score.textContent = `You: ${playerWins} | CPU: ${computerWins}`;
                    roundOutcome.textContent = `${playRound(playerSelection, computerSelection)}`;
                    score.textContent = `You: ${playerWins} | CPU: ${computerWins}`;
                    break;
            }
        });
    }
            

            
}

game();